package ImplementService;

import Service.OrderFunctionUtil;
import enumPackage.OrderState;
import enumPackage.ProductState;
import enumPackage.ProductType;
import model.Order;
import model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class OrderFunctionUtilImpl implements OrderFunctionUtil {

    @Override
    public Boolean isBigOrder(Order order) {
        return order.getProducts().stream().map(Product::getPrice).reduce(Long::sum).filter(a -> a > 10).map(a -> true).orElse(false);
    }

    @Override
    public Boolean hasMultipleProducts(Order order) {
        List<Product> products = order.getProducts();
        products.stream().count();
        Predicate<Order> hasMultipleProduct = a -> a.getProducts().stream().count() > 0;
        return hasMultipleProduct.test(order);
    }

    @Override
    public Boolean hasUnavailableProduct(Order order, Predicate<ProductState> productStatePredicate) {
        return order.getProducts().stream().map(Product::getProductState).anyMatch(productStatePredicate);
    }

    @Override
    public Boolean isReadyToDeliver(Order order, Predicate<ProductState> productPredicate, Predicate<OrderState> orderStatePredicate) {
        boolean allMatch = order.getProducts().stream().map(Product::getProductState).allMatch(productPredicate);
        return Optional.of(order).map(Order::getOrderState).filter(orderStatePredicate)
                .map(orderState -> allMatch).orElse(false);
    }

    @Override
    public Boolean hasPerishableProduct(Order order, Predicate<ProductType> productTypePredicate) {
        return order.getProducts().stream().map(Product::getProductType).anyMatch(productTypePredicate);

    }

    @Override
    public Boolean hasExpensiveBreakableProduct(Order order, Predicate<Product> productPredicate) {

        //method 1
        long count1 = order.getProducts().stream().filter(productPredicate).count();
        //method 2
       // long count2 = order.getProducts().stream().filter(product -> product.getPrice() > 20 && productTypePredicate.test(product.getProductType())).count();
        System.out.println(count1);
        if (count1 > 0) {
            return true;
        } else {
            return false;
        }
    }
}
