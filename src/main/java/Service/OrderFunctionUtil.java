package Service;

import enumPackage.OrderState;
import enumPackage.ProductState;
import enumPackage.ProductType;
import model.Order;
import model.Product;

import java.util.function.Function;
import java.util.function.Predicate;

public interface OrderFunctionUtil {

    Boolean isBigOrder(Order order);

    Boolean hasMultipleProducts(Order order);

    Boolean hasUnavailableProduct(Order order, Predicate<ProductState> productStatePredicate);

    Boolean isReadyToDeliver(Order order, Predicate<ProductState> productStatePredicate, Predicate<OrderState> orderStatePredicate);

    Boolean hasPerishableProduct(Order order, Predicate<ProductType> productTypePredicate);

    Boolean hasExpensiveBreakableProduct(Order order, Predicate<Product> productPredicate);
}
