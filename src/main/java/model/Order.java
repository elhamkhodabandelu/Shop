package model;

import com.sun.org.apache.bcel.internal.generic.NEW;
import enumPackage.OrderState;

import java.util.ArrayList;
import java.util.List;

public class Order {

    private String code;
    private long price;
    private String buyyer;
    OrderState   orderState;
    List<Product> products = new ArrayList<Product>();

    public Order(List<Product> products) {
        this.products = products;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getBuyyer() {
        return buyyer;
    }

    public void setBuyyer(String buyyer) {
        this.buyyer = buyyer;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Order(List<Product> products,OrderState orderState ) {
        this.orderState = orderState;
        this.products = products;
    }
}
