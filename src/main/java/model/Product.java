package model;

import enumPackage.ProductState;
import enumPackage.ProductType;

public class Product {

    private String code;
    private String title;
    private long price;
    ProductState productState;
    ProductType productType;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public ProductState getProductState() {
        return productState;
    }

    public void setProductState(ProductState productState) {
        this.productState = productState;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public Product(long price) {
        this.price = price;
    }

    public Product(String code, ProductState productState) {
        this.code = code;
        this.productState = productState;
    }

    public Product(String code, ProductType productType) {
        this.code = code;
        this.productType = productType;
    }

    public Product(long price, ProductType productType) {
        this.price = price;
        this.productType = productType;
    }
}
