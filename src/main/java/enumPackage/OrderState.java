package enumPackage;

public enum OrderState {
    CONFIRMED, PAID, WAREHOUSE_PROCESSED, READY_TO_SEND, DELIVERED
}
