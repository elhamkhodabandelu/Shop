package ShopTest;

import ImplementService.OrderFunctionUtilImpl;
import Service.OrderFunctionUtil;
import enumPackage.OrderState;
import enumPackage.ProductState;
import enumPackage.ProductType;
import model.Order;
import model.Product;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ShopTest {

    private OrderFunctionUtil orderFunctionUtil = new OrderFunctionUtilImpl(); // TODO: 4/30/2018 new an implemented service
    private Order order;
    private Order orderByProductState;
    private Order orderByOrderState;
    private Order orderByProductType;

    @Before
    public void initTest() {
        order = TestDataUtil.generateOrder();
        orderByProductState = TestDataUtil.generateOrderByProductState();
        orderByOrderState = TestDataUtil.generateOrderByOrderState();
        orderByProductType = TestDataUtil.generateOrderByProductType();


    }

    @Test
    public void success_is_big_order() {
        Boolean bigOrder = orderFunctionUtil.isBigOrder(order);
        System.out.println(bigOrder);
        assertThat(bigOrder, equalTo(true));
    }

    @Test
    public void success_has_multiple_products() {
        Boolean hasMultipleProducts = orderFunctionUtil.hasMultipleProducts(order);
        System.out.println(hasMultipleProducts);
        assertThat(hasMultipleProducts, equalTo(true));
    }

    @Test
    public void success_has_unavailable_product() {
        Boolean hasUnavailableProduct = orderFunctionUtil.hasUnavailableProduct(orderByProductState, a -> a.equals(ProductState.UNAVAILABLE));
        assertThat(hasUnavailableProduct, equalTo(true));
    }

    @Test
    public void success_is_ready_to_deliver() {
        Boolean isReadyToDeliver = orderFunctionUtil.isReadyToDeliver(orderByOrderState, a -> a.equals(ProductState.AVAILABLE), b->b.equals(OrderState.READY_TO_SEND));
        assertThat(isReadyToDeliver, equalTo(true));
    }

    @Test
    public void success_has_perishable_product() {
        Boolean hasPerishableProduct = orderFunctionUtil.hasPerishableProduct(orderByProductType, a -> a.equals(ProductType.PERISHABLE));
        assertThat(hasPerishableProduct, equalTo(true));
    }

    @Test
    public void success_has_Expensive_Breakable_Productnction_Util(){
      Boolean hasExpensiveBreakableProduct  = orderFunctionUtil.hasExpensiveBreakableProduct(orderByProductType, a -> a.getProductType().equals(ProductType.PERISHABLE) && a.getPrice() >20 );
        assertThat(hasExpensiveBreakableProduct, equalTo(true));
    }


}
