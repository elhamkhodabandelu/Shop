package ShopTest;

import enumPackage.OrderState;
import enumPackage.ProductState;
import enumPackage.ProductType;
import model.Order;
import model.Product;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

public class TestDataUtil {

    public static Order generateOrder() {

        List<Product> products = getProductList();
        Function<List<Product>, Order> function = Order::new;
        return function.apply(products);
    }


    public static Order generateOrderByProductState() {

        List<Product> products = getProductListByProductState();
        Function<List<Product>, Order> function = Order::new;
        return function.apply(products);
    }


    public static List<Product> getProductList() {


        Function<Long, Product> function = Product::new;
        return Arrays.asList(
                function.apply(5L),
                function.apply(8L),
                function.apply(40L),
                function.apply(50L),
                function.apply(60L)

        );
    }


    public static List<Product> getProductListByProductState() {


        BiFunction<String, ProductState, Product> function = Product::new;
        return Arrays.asList(
                function.apply("1", ProductState.AVAILABLE),
                function.apply("5", ProductState.UNAVAILABLE),
                function.apply("4", ProductState.AVAILABLE),
                function.apply("3", ProductState.UNAVAILABLE),
                function.apply("2", ProductState.AVAILABLE)


        );
    }


    public static List<Product> getProductListBySameProductState() {
        BiFunction<String, ProductState, Product> function = Product::new;
        return Arrays.asList(
                function.apply("1", ProductState.AVAILABLE),
                function.apply("5", ProductState.AVAILABLE),
                function.apply("4", ProductState.AVAILABLE),
                function.apply("3", ProductState.AVAILABLE),
                function.apply("2", ProductState.AVAILABLE)


        );
    }

    public static Order generateOrderByOrderState() {

        List<Product> products = getProductList();
        List<Product> productsBySameProductState = getProductListBySameProductState();
        BiFunction<List<Product>, OrderState, Order> function = Order::new;
        return
                function.apply(productsBySameProductState, OrderState.READY_TO_SEND);
        //function.apply(products,OrderState.PAID);
    }

    public static Order generateOrderByProductType() {

        List<Product> products = getProductListByProductType();
        Function<List<Product>, Order> function = Order::new;
        return
                function.apply(products);
    }


    public static List<Product> getProductListByProductType() {
        BiFunction<Long, ProductType, Product> function = Product::new;
        return Arrays.asList(
                function.apply(2L,ProductType.BREAKABLE ),
                function.apply(10L,ProductType.NORMAL ),
                function.apply(5L,ProductType.BREAKABLE ),
                function.apply(30L,ProductType.PERISHABLE ),
                function.apply(40L,ProductType.NORMAL )

        );
    }

}
